-- conectamos a la db.
\c grupo_5

-- creacion de la tabla localidad
CREATE TABLE localidad(
	id serial PRIMARY KEY,
	nombre text UNIQUE
);

-- creacion de la tabla tipocontacto
CREATE TABLE tipocontacto(
	id serial PRIMARY KEY,
	nombre text UNIQUE
);

-- creacion de la tabla personas
CREATE TABLE personas(
	idPersona serial PRIMARY KEY,
	nombre varchar(50),
	calle varchar(50),
	altura varchar(50),
	piso varchar(50),
	dpto varchar(10),
	localidad text references localidad(nombre),
	email varchar(50),
	fechacumple date,
	tipocontacto text references tipocontacto(nombre),
	telefono text
);