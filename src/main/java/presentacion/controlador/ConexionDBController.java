package presentacion.controlador;

import java.awt.event.ActionEvent;

import org.jfree.util.Log;

import dto.ConexionDBDTO;
import persistencia.conexion.Conexion;
import persistencia.conexion.PersistirJSON;
import presentacion.vista.VentanaConexionDB;

public class ConexionDBController
{
	private VentanaConexionDB ventanaConexionDB;
	
	public ConexionDBController()
	{
		this.ventanaConexionDB = VentanaConexionDB.getInstance();
		
		this.ventanaConexionDB.getBtnProbarConexion().addActionListener(pc->validarConexion(pc));
		this.ventanaConexionDB.getBtnGuardarYConectar().addActionListener(gyc->guardarDatosDB(gyc));
	}
	
	public boolean iniciarPrecondicionesDB()
	{
		// verificamos si el archivo JSON existe.
		if (PersistirJSON.getInstance().existeArchivoJSON())
		{
			ConexionDBDTO conexion = PersistirJSON.getInstance().obtenerDatos();
			// verificamos si es posible conectarse a la DB.
			if (Conexion.esPosibleConectar(conexion))
			{
				Log.info("¡OK! conexion exitosa.");
				System.out.println(conexion.getUsuario() + " | " + conexion.getContrasenia() + " | " + conexion.getDireccion() + " | " + conexion.getPuerto() + " ! ");
				return true;
			}
			else
				return false;
		}
		return false;
	}
	
	public void mostrarVentanaInicialConfig()
	{
		this.ventanaConexionDB.conexionFallida();
		this.ventanaConexionDB.mostrarVentana();
	}
	
	private void validarConexion(ActionEvent pc)
	{
		String direccion = this.ventanaConexionDB.getTxtDireccion().getText();
		String puerto = this.ventanaConexionDB.getTxtPuerto().getText();
		String usuario = this.ventanaConexionDB.getTxtUsuario().getText();
		String contrasenia = this.ventanaConexionDB.getTxtContrasenia().getText();
		
		if (direccion == null || direccion.isEmpty() || puerto == null || puerto.isEmpty() || usuario == null || usuario.isEmpty() || contrasenia == null || contrasenia.isEmpty())
			this.ventanaConexionDB.faltanDatosError();
		else
		{
			ConexionDBDTO nuevaConexion = new ConexionDBDTO(this.ventanaConexionDB.getTxtDireccion().getText(), this.ventanaConexionDB.getTxtPuerto().getText(), this.ventanaConexionDB.getTxtUsuario().getText(), this.ventanaConexionDB.getTxtContrasenia().getText());
			
			if (Conexion.esPosibleConectar(nuevaConexion))
				this.ventanaConexionDB.conexionExitosa();
			else
				this.ventanaConexionDB.conexionFallida();	
		}
	}
	private void guardarDatosDB(ActionEvent gyc)
	{
		String direccion = this.ventanaConexionDB.getTxtDireccion().getText();
		String puerto = this.ventanaConexionDB.getTxtPuerto().getText();
		String usuario = this.ventanaConexionDB.getTxtUsuario().getText();
		String contrasenia = this.ventanaConexionDB.getTxtContrasenia().getText();
		
		if (direccion == null || direccion.isEmpty() || puerto == null || puerto.isEmpty() || usuario == null || usuario.isEmpty() || contrasenia == null || contrasenia.isEmpty())
			this.ventanaConexionDB.faltanDatosError();
		else
		{
			ConexionDBDTO nuevaConexion = new ConexionDBDTO(this.ventanaConexionDB.getTxtDireccion().getText(), this.ventanaConexionDB.getTxtPuerto().getText(), this.ventanaConexionDB.getTxtUsuario().getText(), this.ventanaConexionDB.getTxtContrasenia().getText());
			if (Conexion.esPosibleConectar(nuevaConexion))
			{
				if (PersistirJSON.getInstance().guardarEnDB(direccion, puerto, usuario, contrasenia))
				{
					this.ventanaConexionDB.todoListo();
					this.ventanaConexionDB.cerrar();
				}
				else
					this.ventanaConexionDB.ocurrioUnProblemaBD();
			}
			else
				this.ventanaConexionDB.conexionFallida();
		}
	}
}