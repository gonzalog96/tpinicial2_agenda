package presentacion.controlador;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import modelo.Agenda;
import persistencia.conexion.Conexion;
import persistencia.conexion.PersistirJSON;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaConexionDB;
import presentacion.vista.VentanaEdicionContacto;
import presentacion.vista.VentanaEdicionLocalidad;
import presentacion.vista.VentanaEdicionTipoDeContacto;
import presentacion.vista.VentanaNuevaLocalidad;
import presentacion.vista.VentanaNuevoContacto;
import presentacion.vista.VentanaNuevoTipoDeContacto;
import presentacion.vista.Vista;
import dto.ConexionDBDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

public class Controlador implements ActionListener
{
		private Vista vista;
		
		private List<PersonaDTO> personasEnTabla;
		private List<LocalidadDTO> localidadesEnTabla;
		private List<TipoContactoDTO> tipoDeContactosEnTabla;
		
		private VentanaNuevoContacto ventanaNuevoContacto; 
		private VentanaEdicionContacto ventanaEdicionContacto;
		
		private VentanaNuevaLocalidad ventanaNuevaLocalidad;
		private VentanaEdicionLocalidad ventanaEdicionLocalidad;
		
		private VentanaNuevoTipoDeContacto ventanaNuevoTipoDeContacto;
		private VentanaEdicionTipoDeContacto ventanaEdicionTipoDeContacto;
		
		private VentanaConexionDB ventanaConexionDB;
		
		private Agenda agenda;
		
		public Controlador(Vista vista, Agenda agenda)
		{
			this.vista = vista;
			
			/** para el caso de los contactos */
			this.vista.getBtnAgregarContacto().addActionListener(a->ventanaAgregarContacto(a));
			this.vista.getBtnEditarContacto().addActionListener(e->ventanaEditarContacto(e));
			this.vista.getBtnBorrarContacto().addActionListener(s->borrarContacto(s));
			
			// ventana para agregar nuevos contactos.
			this.ventanaNuevoContacto = VentanaNuevoContacto.getInstance();
			this.ventanaNuevoContacto.getBtnAgregarPersona().addActionListener(p->añadirNuevoContacto(p));
			
			// ventana para editar contactos.
			this.ventanaEdicionContacto = VentanaEdicionContacto.getInstance();
			this.ventanaEdicionContacto.getBtnGuardarCambios().addActionListener(g->guardarCambiosEnContacto(g));
			
			/** para el caso de las localidades */
			this.vista.getBtnAñadirLocalidad().addActionListener(al->ventanaAgregarLocalidad(al));
			this.vista.getBtnEditarLocalidad().addActionListener(el->ventanaEditarLocalidad(el));
			this.vista.getBtnBorrarLocalidad().addActionListener(bl->borrarLocalidad(bl));
			
			// ventana para agregar nuevas localidades.
			this.ventanaNuevaLocalidad = VentanaNuevaLocalidad.getInstance();
			this.ventanaNuevaLocalidad.getBtnAgregarLocalidad().addActionListener(al->añadirNuevaLocalidad(al));
			
			// ventana para editar las localidades.
			this.ventanaEdicionLocalidad = VentanaEdicionLocalidad.getInstance();
			this.ventanaEdicionLocalidad.getBtnEditarLocalidad().addActionListener(el->guardarCambiosEnLocalidad(el));
			
			/** para el caso de los tipos de contacto */
			this.vista.getBtnAñadirTipoDeContacto().addActionListener(atc->ventanaAgregarTipoDeContacto(atc));
			this.vista.getBtnEditarTipoDeContacto().addActionListener(etc->ventanaEditarTipoDeContacto(etc));
			this.vista.getBtnBorrarTipoDeContacto().addActionListener(btc->borrarTipoDeContacto(btc));
			
			// ventana para agregar nuevos tipos de contactos.
			this.ventanaNuevoTipoDeContacto = VentanaNuevoTipoDeContacto.getInstance();
			this.ventanaNuevoTipoDeContacto.getBtnTipoDeContacto().addActionListener(atc->añadirNuevoTipoDeContacto(atc));
			
			// ventana para editar tipos de contactos.
			this.ventanaEdicionTipoDeContacto = VentanaEdicionTipoDeContacto.getInstance();
			this.ventanaEdicionTipoDeContacto.getBtnEditarTipoDeContacto().addActionListener(etc->guardarCambiosEnTipoDeContacto(etc));
			
			/** para el caso de las DB */
			this.ventanaConexionDB = VentanaConexionDB.getInstance();
			this.vista.getBtnDbPrueba().addActionListener(db->ventanaDatosConexionDB(db));
			
			this.ventanaConexionDB.getBtnProbarConexion().addActionListener(pc->validarConexion(pc));
			this.ventanaConexionDB.getBtnGuardarYConectar().addActionListener(gyc->guardarDatosDB(gyc));
			
			/** reportes */
			this.vista.getBtnGenerarReporte().addActionListener(r->mostrarReporte(r));
			
			this.agenda = agenda;
		}
		
		// seccion para mostrar las ventanas.
		
		private void ventanaAgregarContacto(ActionEvent a) 
		{
			this.ventanaNuevoContacto.mostrarVentana();
			
			this.ventanaNuevoContacto.cargarLocalidades(this.agenda.obtenerLocalidades());
			this.ventanaNuevoContacto.cargarTiposDeContacto(this.agenda.obtenerTiposDeContacto());
		}
		private void ventanaAgregarLocalidad(ActionEvent al)
		{
			this.ventanaNuevaLocalidad.mostrarVentana();
		}
		private void ventanaAgregarTipoDeContacto(ActionEvent atc)
		{
			this.ventanaNuevoTipoDeContacto.mostrarVentana();
		}

		// seccion para nuevas creaciones.
		
		private void añadirNuevoContacto(ActionEvent p) 
		{
			String nombre = this.ventanaNuevoContacto.getTxtNombre().getText();
			
			String calle = this.ventanaNuevoContacto.getTxtCalle().getText();
			String altura = this.ventanaNuevoContacto.getTxtAltura().getText();
			String piso = this.ventanaNuevoContacto.getTxtPiso().getText();
			String dpto = this.ventanaNuevoContacto.getTxtDpto().getText();
			String localidad = this.ventanaNuevoContacto.getTxtLocalidad();
			String email = this.ventanaNuevoContacto.getTxtEmail().getText();
			Date fechaCumple = this.ventanaNuevoContacto.getTxtCumple();
			String tipoContacto = this.ventanaNuevoContacto.getTxtTipoContacto();
			
			String tel = ventanaNuevoContacto.getTxtTelefono().getText();
			
			if ((nombre == null || nombre.isEmpty()) || (calle == null || calle.isEmpty()) || (altura == null || altura.isEmpty()) || (localidad == null || localidad.isEmpty()) || fechaCumple == null || tipoContacto == null)
				this.ventanaNuevoContacto.faltanDatosError();
			
			else
			{
				PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, calle, altura, piso, dpto, localidad, email, fechaCumple, tipoContacto, tel);
				
				if (this.agenda.personaExisteEnDB(nuevaPersona))
					this.ventanaNuevoContacto.contactoExistenteError();
				else
				{
					this.agenda.agregarPersona(nuevaPersona);
					this.refrescarTabla();
					this.ventanaNuevoContacto.cerrar();
				}
			}
		}
		private void añadirNuevaLocalidad(ActionEvent al)
		{
			String nombreLocalidad = this.ventanaNuevaLocalidad.getLocalidad().getText();
			
			if (nombreLocalidad.isEmpty())
				this.ventanaNuevaLocalidad.faltanDatosError();
			else
			{
				LocalidadDTO nuevaLocalidad = new LocalidadDTO(0, nombreLocalidad);
				
				if (this.agenda.localidadExisteEnDB(nuevaLocalidad))
					this.ventanaNuevaLocalidad.localidadRepetida();
				
				else
				{
					this.agenda.agregarNuevaLocalidad(nuevaLocalidad);
					this.refrescarTabla();
					this.ventanaNuevaLocalidad.cerrar();	
				}
			}
		}
		private void añadirNuevoTipoDeContacto(ActionEvent atc)
		{
			String nombreTipoDeContacto = this.ventanaNuevoTipoDeContacto.getTipoDeContacto().getText();
			
			if (nombreTipoDeContacto.isEmpty())
				this.ventanaNuevoTipoDeContacto.faltanDatosError();
			else
			{
				TipoContactoDTO nuevoTipoDeContacto = new TipoContactoDTO(0, nombreTipoDeContacto);
				
				if (this.agenda.tipoDeContactoExisteEnDB(nuevoTipoDeContacto))
					this.ventanaNuevoTipoDeContacto.tipoDeContactoRepetido();
				else
				{
					this.agenda.agregarNuevoTipoDeContacto(nuevoTipoDeContacto);
					this.refrescarTabla();
					this.ventanaNuevoTipoDeContacto.cerrar();
				}
			}
		}
		
		// seccion de ediciones.
		
		private void ventanaEditarContacto(ActionEvent e)
		{
			if (this.vista.getTablaPersonas().getSelectedRow() == -1)
				this.vista.faltaSeleccionItemError();
			else
			{
				PersonaDTO datosContacto = this.personasEnTabla.get(this.vista.getTablaPersonas().getSelectedRow());
				
				this.ventanaEdicionContacto.precargarDatos(datosContacto.getNombre(), datosContacto.getCalle(), datosContacto.getAltura(), datosContacto.getPiso(), datosContacto.getDpto(), datosContacto.getLocalidad(), datosContacto.getEmail(), datosContacto.getFechaCumple(), datosContacto.getTipoContacto(), datosContacto.getTelefono());
				this.ventanaEdicionContacto.cargarLocalidades(this.agenda.obtenerLocalidades(), datosContacto.getLocalidad());
				this.ventanaEdicionContacto.cargarTiposDeContacto(this.agenda.obtenerTiposDeContacto(), datosContacto.getTipoContacto());
				
				this.ventanaEdicionContacto.mostrar();
			}
		}
		private void ventanaEditarLocalidad(ActionEvent el)
		{
			if (this.vista.getTablaLocalidades().getSelectedRow() == -1)
				this.vista.faltaSeleccionItemError();
			else
			{
				LocalidadDTO datosLocalidad = this.localidadesEnTabla.get(this.vista.getTablaLocalidades().getSelectedRow());
				
				this.ventanaEdicionLocalidad.precargarDatos(datosLocalidad.getDescripcion());
				this.ventanaEdicionLocalidad.mostrar();	
			}
		}
		private void ventanaEditarTipoDeContacto(ActionEvent etc)
		{
			if (this.vista.getTablaTiposDeContacto().getSelectedRow() == -1)
				this.vista.faltaSeleccionItemError();
			else
			{
				TipoContactoDTO datosTipoDeContacto = this.tipoDeContactosEnTabla.get(this.vista.getTablaTiposDeContacto().getSelectedRow());
				
				this.ventanaEdicionTipoDeContacto.precargarDatos(datosTipoDeContacto.getConcepto());
				this.ventanaEdicionTipoDeContacto.mostrar();
			}
		}
		
		// seccion para guardar cambios.
		
		private void guardarCambiosEnContacto(ActionEvent g)
		{
			String nombre = this.ventanaEdicionContacto.getNombre().getText();
			
			String calle = this.ventanaEdicionContacto.getCalle().getText();
			String altura = this.ventanaEdicionContacto.getAltura().getText();
			String piso = this.ventanaEdicionContacto.getPiso().getText();
			String dpto = this.ventanaEdicionContacto.getDpto().getText();
			String localidad = this.ventanaEdicionContacto.getLocalidad();
			String email = this.ventanaEdicionContacto.getEmail().getText();
			Date fechaCumple = this.ventanaEdicionContacto.getCumple();
			String tipoContacto = this.ventanaEdicionContacto.getTipoContacto();
			String tel = ventanaEdicionContacto.getTelefono().getText();
			
			if ((nombre == null || nombre.isEmpty()) || (calle == null || calle.isEmpty()) || (altura == null || altura.isEmpty()) || (localidad == null || localidad.isEmpty()) || fechaCumple == null || tipoContacto == null)
				this.ventanaEdicionContacto.faltanDatosEnContacto();
			else
			{
				this.agenda.borrarPersona(this.personasEnTabla.get(this.vista.getTablaPersonas().getSelectedRow()));
				
				PersonaDTO personaModificada = new PersonaDTO(0, nombre, calle, altura, piso, dpto, localidad, email, fechaCumple, tipoContacto, tel);
				this.agenda.agregarPersona(personaModificada);
				
				this.refrescarTabla();
				this.ventanaEdicionContacto.cerrar();
			}
		}
		private void guardarCambiosEnLocalidad(ActionEvent gcl)
		{
			String descripcionLocalidad = this.ventanaEdicionLocalidad.getNombreLocalidad().getText();
			
			if (descripcionLocalidad.isEmpty())
				this.ventanaEdicionLocalidad.faltaDescripcion();
			
			else if (this.agenda.localidadExisteEnDB(new LocalidadDTO(0, descripcionLocalidad)))
				this.ventanaEdicionLocalidad.tipoDeLocalidadRepetida();
			else
			{
				LocalidadDTO localidadAntigua = this.localidadesEnTabla.get(this.vista.getTablaLocalidades().getSelectedRow());
				
				LocalidadDTO localidadModificada = new LocalidadDTO(0, descripcionLocalidad);
				this.agenda.agregarNuevaLocalidad(localidadModificada);
				
				// actualizamos los datos de TODOS los contactos que tenian la antigua localidad
				this.agenda.actualizarDatosLocalidad(localidadAntigua.getDescripcion(), localidadModificada.getDescripcion());
				
				this.agenda.borrarLocalidad(localidadAntigua);
				
				this.refrescarTabla();
				this.ventanaEdicionLocalidad.cerrar();
			}
		}
		private void guardarCambiosEnTipoDeContacto(ActionEvent gctc)
		{
			String descripcionTipoDeContacto = this.ventanaEdicionTipoDeContacto.getNombreTipoDeContacto().getText();
			
			if (descripcionTipoDeContacto.isEmpty())
				this.ventanaEdicionTipoDeContacto.faltaDescripcionTipoDeContacto();
			
			else if (this.agenda.tipoDeContactoExisteEnDB(new TipoContactoDTO(0, descripcionTipoDeContacto)))
				this.ventanaEdicionTipoDeContacto.tipoDeContactoRepetido();
			else
			{
				TipoContactoDTO tipoContactoViejo = this.tipoDeContactosEnTabla.get(this.vista.getTablaTiposDeContacto().getSelectedRow());
				
				TipoContactoDTO tipoDeContactoModificado = new TipoContactoDTO(0, descripcionTipoDeContacto);
				this.agenda.agregarNuevoTipoDeContacto(tipoDeContactoModificado);
				
				// actualizamos los datos de TODOS los contactos que tenian el antiguo tipo de contacto
				this.agenda.actualizarDatosTipoDeContacto(tipoContactoViejo.getConcepto(), tipoDeContactoModificado.getConcepto());
				
				this.agenda.borrarTipoDeContacto(tipoContactoViejo);
				
				this.refrescarTabla();
				this.ventanaEdicionTipoDeContacto.cerrar();	
			}
		}

		// seccion para borrar objetos.
		
		public void borrarContacto(ActionEvent s)
		{
			if (this.vista.getTablaPersonas().getSelectedRow() == -1)
				this.vista.faltaSeleccionItemError();
			else
			{
				int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
				for (int fila : filasSeleccionadas)
				{
					this.agenda.borrarPersona(this.personasEnTabla.get(fila));
				}
				
				this.refrescarTabla();	
			}
		}
		public void borrarLocalidad(ActionEvent bl)
		{
			if (this.vista.getTablaLocalidades().getSelectedRow() == -1)
				this.vista.faltaSeleccionItemError();
			else if (this.localidadesEnTabla.get(this.vista.getTablaLocalidades().getSelectedRow()).getDescripcion().equals("Ninguna"))
				this.vista.errorImposibleEliminarItem();
			else
			{
				int[] filasSeleccionadas = this.vista.getTablaLocalidades().getSelectedRows();
				List<LocalidadDTO> localidades = new ArrayList<LocalidadDTO>();
				
				for (int fila : filasSeleccionadas)
				{
					LocalidadDTO localidad = this.localidadesEnTabla.get(fila);
					localidades.add(localidad);
					
					this.agenda.actualizarDatosLocalidad(localidad.getDescripcion());
				}
				
				for (LocalidadDTO localidad : localidades)
				{
					this.agenda.borrarLocalidad(localidad);
				}
				
				this.refrescarTabla();
			}
		}
		public void borrarTipoDeContacto(ActionEvent btc)
		{
			if (this.vista.getTablaTiposDeContacto().getSelectedRow() == -1)
				this.vista.faltaSeleccionItemError();
			else if (this.tipoDeContactosEnTabla.get(this.vista.getTablaTiposDeContacto().getSelectedRow()).getConcepto().equals("Ninguno"))
				this.vista.errorImposibleEliminarItem();
			else
			{
				int[] filasSeleccionadas = this.vista.getTablaTiposDeContacto().getSelectedRows();
				List<TipoContactoDTO> tiposDeContacto = new ArrayList<TipoContactoDTO>();
				
				for (int fila : filasSeleccionadas)
				{
					TipoContactoDTO tipoContacto = this.tipoDeContactosEnTabla.get(fila);
					tiposDeContacto.add(tipoContacto);
					
					this.agenda.actualizarDatosTipoDeContacto(tipoContacto.getConcepto());
				}
				
				for (TipoContactoDTO tipoDeContacto : tiposDeContacto)
				{
					this.agenda.borrarTipoDeContacto(tipoDeContacto);	
				}
				
				this.refrescarTabla();
			}
		}
		
		private void ventanaDatosConexionDB(ActionEvent db)
		{
			ConexionDBDTO conexion = PersistirJSON.getInstance().obtenerDatos();
			
			this.ventanaConexionDB.precargarDatos(conexion.getDireccion(), conexion.getPuerto(), conexion.getUsuario(), conexion.getContrasenia());
			this.ventanaConexionDB.mostrarVentana();
		}
		private void validarConexion(ActionEvent pc)
		{
			String direccion = this.ventanaConexionDB.getTxtDireccion().getText();
			String puerto = this.ventanaConexionDB.getTxtPuerto().getText();
			String usuario = this.ventanaConexionDB.getTxtUsuario().getText();
			String contrasenia = this.ventanaConexionDB.getTxtContrasenia().getText();
			
			if (direccion == null || direccion.isEmpty() || puerto == null || puerto.isEmpty() || usuario == null || usuario.isEmpty() || contrasenia == null || contrasenia.isEmpty())
				this.ventanaConexionDB.faltanDatosError();
			else
			{
				ConexionDBDTO nuevaConexion = new ConexionDBDTO(this.ventanaConexionDB.getTxtDireccion().getText(), this.ventanaConexionDB.getTxtPuerto().getText(), this.ventanaConexionDB.getTxtUsuario().getText(), this.ventanaConexionDB.getTxtContrasenia().getText());
				
				if (Conexion.esPosibleConectar(nuevaConexion))
					this.ventanaConexionDB.conexionExitosa();
				else
					this.ventanaConexionDB.conexionFallida();	
			}
		}
		private void guardarDatosDB(ActionEvent gyc)
		{
			String direccion = this.ventanaConexionDB.getTxtDireccion().getText();
			String puerto = this.ventanaConexionDB.getTxtPuerto().getText();
			String usuario = this.ventanaConexionDB.getTxtUsuario().getText();
			String contrasenia = this.ventanaConexionDB.getTxtContrasenia().getText();
			
			if (direccion == null || direccion.isEmpty() || puerto == null || puerto.isEmpty() || usuario == null || usuario.isEmpty() || contrasenia == null || contrasenia.isEmpty())
				this.ventanaConexionDB.faltanDatosError();
			else
			{
				ConexionDBDTO nuevaConexion = new ConexionDBDTO(this.ventanaConexionDB.getTxtDireccion().getText(), this.ventanaConexionDB.getTxtPuerto().getText(), this.ventanaConexionDB.getTxtUsuario().getText(), this.ventanaConexionDB.getTxtContrasenia().getText());
				if (Conexion.esPosibleConectar(nuevaConexion))
				{
					PersistirJSON.getInstance().guardarEnDB(direccion, puerto, usuario, contrasenia);
					this.ventanaConexionDB.ventanaDatosGuardados();
					this.ventanaConexionDB.cerrar();
				}
				else
				{
					this.ventanaConexionDB.conexionFallida();
					this.ventanaConexionDB.cerrar();
				}
			}
		}
		
		private void mostrarReporte(ActionEvent r) 
		{
			if (agenda.obtenerPersonasParaReporte().size() == 0)
				this.vista.errorSinPersonasParaReporte();
			else
			{
				ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonasParaReporte());
				reporte.mostrar();
			}
		}
		
		public void inicializar()
		{
			this.refrescarTabla();
			this.vista.show();
		}
		
		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerPersonas();
			this.localidadesEnTabla = agenda.obtenerLocalidades();
			this.tipoDeContactosEnTabla = agenda.obtenerTiposDeContacto();
	
			this.vista.llenarTablaContactos(this.personasEnTabla);
			this.vista.llenarTablaLocalidades(this.localidadesEnTabla);
			this.vista.llenarTablaTiposDeContacto(this.tipoDeContactosEnTabla);
		}

		@Override
		public void actionPerformed(ActionEvent e) { }
}