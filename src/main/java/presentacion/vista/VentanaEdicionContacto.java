package presentacion.vista;


import java.awt.Color;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import dto.LocalidadDTO;
import dto.TipoContactoDTO;

public class VentanaEdicionContacto extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtNombre;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDpto;
	private JTextField txtEmail;
	private JTextField txtTelefono;
	
	private JComboBox<String> localidad;
	private JComboBox<String> tipoContacto;
	private JDateChooser fechaCumple;
	
	private JButton btnGuardarDatos;
	
	private static VentanaEdicionContacto INSTANCE;
	
	public static VentanaEdicionContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaEdicionContacto(); 	
			return new VentanaEdicionContacto();
		}
		else
			return INSTANCE;
	}

	private VentanaEdicionContacto() 
	{
		super();
		try
		{
			setIconImage(ImageIO.read(getClass().getResourceAsStream("favicon.png")));
			setTitle("TP inicial II - Agenda (ABM) - LCS - UNGS 02/2019 - Godoy");
		} catch (IOException e){ e.printStackTrace(); }
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 378);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 404, 337);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 11, 113, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 36, 113, 14);
		panel.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 61, 113, 14);
		panel.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(10, 86, 113, 14);
		panel.add(lblPiso);
		
		JLabel lblDpto = new JLabel("Dpto");
		lblDpto.setBounds(10, 111, 113, 14);
		panel.add(lblDpto);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 136, 113, 14);
		panel.add(lblLocalidad);

		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setBounds(10, 171, 113, 14);
		panel.add(lblEmail);
		
		JLabel lblFechaCumple = new JLabel("Fecha de cumpleaños");
		lblFechaCumple.setBounds(10, 207, 131, 14);
		panel.add(lblFechaCumple);
		
		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setBounds(10, 232, 113, 14);
		panel.add(lblTelefono);
		
		JLabel lblTipoContacto = new JLabel("Tipo de contacto");
		lblTipoContacto.setBounds(10, 257, 113, 14);
		panel.add(lblTipoContacto);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(165, 8, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtCalle = new JTextField();
		txtCalle.setBounds(165, 33, 164, 20);
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		txtAltura = new JTextField();
		txtAltura.setBounds(165, 58, 164, 20);
		panel.add(txtAltura);
		txtAltura.setColumns(10);
		
		txtPiso = new JTextField();
		txtPiso.setBounds(165, 83, 164, 20);
		panel.add(txtPiso);
		txtPiso.setColumns(10);
		
		txtDpto = new JTextField();
		txtDpto.setBounds(165, 108, 164, 20);
		panel.add(txtDpto);
		txtDpto.setColumns(10);
		
		localidad = new JComboBox<String>();
		localidad.setBounds(165, 133, 146, 20);
		panel.add(localidad);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(165, 168, 164, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		fechaCumple = new JDateChooser();
		fechaCumple.setBackground(Color.RED);
		fechaCumple.setLocale(new Locale("es"));
		fechaCumple.setDateFormatString("yyyy-MM-dd");
		fechaCumple.getJCalendar().setMaxSelectableDate(new Date());
		fechaCumple.setBounds(165, 201, 164, 20);
		
		JTextFieldDateEditor editor = (JTextFieldDateEditor) fechaCumple.getDateEditor();
		editor.setEditable(false);
		
		panel.add(fechaCumple);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(165, 229, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		tipoContacto = new JComboBox<String>();
		tipoContacto.setBounds(165, 254, 146, 20);
		panel.add(tipoContacto);
		
		btnGuardarDatos = new JButton("Guardar datos");
		btnGuardarDatos.setBounds(132, 285, 146, 28);
		panel.add(btnGuardarDatos);
		
		this.setVisible(false);
	}
	
	public void precargarDatos(String nombre, String calle, String altura, String piso, String dpto, String localidad, String email, Date fechaCumple, String tipoContacto, String telefono) 
	{
		this.txtNombre.setText(nombre);
		this.txtCalle.setText(calle);
		this.txtAltura.setText(altura);
		this.txtPiso.setText(piso);
		this.txtDpto.setText(dpto);
		this.txtEmail.setText(email);
		this.fechaCumple.setDate(fechaCumple);
		this.txtTelefono.setText(telefono);
	}
	
	public void cargarLocalidades(List<LocalidadDTO> localidades, String localidadDefault)
	{
		this.localidad.removeAllItems();
		for(LocalidadDTO l : localidades) 
		{
			this.localidad.addItem(l.getDescripcion());
		}
		this.localidad.setSelectedItem(localidadDefault);
	}
	
	public void cargarTiposDeContacto(List<TipoContactoDTO> tipoDeContactos, String tipoDeContactoDefault)
	{
		this.tipoContacto.removeAllItems();
		for(TipoContactoDTO t : tipoDeContactos) 
		{
			this.tipoContacto.addItem(t.getConcepto());
		}
		this.tipoContacto.setSelectedItem(tipoDeContactoDefault);
	}
	public void faltanDatosEnContacto()
	{
	    String message = "¡Error al actualizar los datos!\n Verificar que los siguientes campos estén completos:\n- Nombre y apellido.\n- Calle.\n- Altura.\n- Localidad.\n- Fecha de cumpleaños.\n- Tipo de contacto.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	public void mostrar()
	{
		this.setVisible(true);
	}
	
	public JTextField getNombre() 
	{
		return txtNombre;
	}

	public JTextField getCalle()
	{
		return txtCalle;
	}

	public JTextField getAltura()
	{
		return txtAltura;
	}

	public JTextField getPiso()
	{
		return txtPiso;
	}

	public JTextField getDpto()
	{
		return txtDpto;
	}

	public JTextField getEmail()
	{
		return txtEmail;
	}

	public String getLocalidad()
	{
		return (String) localidad.getSelectedItem();
	}

	public String getTipoContacto()
	{
		return (String) tipoContacto.getSelectedItem();
	}

	public Date getCumple()
	{
		return fechaCumple.getDate();
	}

	public JTextField getTelefono() 
	{
		return txtTelefono;
	}

	public JButton getBtnGuardarCambios() 
	{
		return btnGuardarDatos;
	}

	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.dispose();
	}
}