package presentacion.vista;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaNuevaLocalidad extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtNombreLocalidad;
	
	private JButton btnAgregarLocalidad;
	
	private static VentanaNuevaLocalidad INSTANCE;
	
	public static VentanaNuevaLocalidad getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaNuevaLocalidad(); 	
			return new VentanaNuevaLocalidad();
		}
		else
			return INSTANCE;
	}
	
	private VentanaNuevaLocalidad() 
	{
		super();
		try
		{
			setIconImage(ImageIO.read(getClass().getResourceAsStream("favicon.png")));
			setTitle("TP inicial II - Agenda (ABM) - LCS - UNGS 02/2019 - Godoy");
		} catch (IOException e){ e.printStackTrace(); }
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 740, 539);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreLocalidad = new JLabel("Nombre de la localidad: ");
		lblNombreLocalidad.setBounds(10, 11, 145, 14);
		panel.add(lblNombreLocalidad);
		
		txtNombreLocalidad = new JTextField();
		txtNombreLocalidad.setBounds(165, 8, 164, 20);
		panel.add(txtNombreLocalidad);
		txtNombreLocalidad.setColumns(10);
		
		btnAgregarLocalidad = new JButton("Añadir localidad");
		btnAgregarLocalidad.setBounds(554, 500, 176, 28);
		panel.add(btnAgregarLocalidad);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.txtNombreLocalidad.setText(null);
		this.setVisible(true);
	}
	
	public void faltanDatosError()
	{
	    String message = "Falta ingresar un nombre para la localidad.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	public void localidadRepetida()
	{
		String message = "La localidad ingresada ya existe en el sistema.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	
	public JTextField getLocalidad() 
	{
		return txtNombreLocalidad;
	}
	
	public JButton getBtnAgregarLocalidad() 
	{
		return btnAgregarLocalidad;
	}
	
	public void cerrar()
	{
		this.txtNombreLocalidad.setText(null);
		this.dispose();
	}
}