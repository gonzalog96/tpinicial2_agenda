package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

import javax.imageio.ImageIO;
import javax.swing.JButton;

import persistencia.conexion.Conexion;

public class Vista
{
	private JFrame frame;
	
	private JTable tablaPersonas;
	private JTable tablaLocalidades;
	private JTable tablaTiposDeContacto;
	
	private JButton btnAgregarContacto;
	private JButton btnEditarContacto;
	private JButton btnBorrarContacto;
	
	private JButton btnAñadirLocalidad;
	private JButton btnEditarLocalidad;
	private JButton btnBorrarLocalidad;
	
	private JButton btnAñadirTipoDeContacto;
	private JButton btnEditarTipoDeContacto;
	private JButton btnBorrarTipoDeContacto;
	
	private JButton btnGenerarReporte;
	
	private DefaultTableModel modelPersonas;
	private DefaultTableModel modelLocalidades;
	private DefaultTableModel modelTiposDeContacto;
	
	private String[] nombreColumnas = {"Nombre y apellido", "Calle", "Altura", "Piso", "Dpto", "Localidad", "E-mail", "Cumpleaños", "Tipo de contacto", "Telefono"};
	private String[] localidadColumna = {"Localidades"};
	private String[] tiposDeContactoColumna = {"Tipos de contacto"};
	private JButton btnDbPrueba;
	
	public Vista() 
	{
		super();
		initialize();
	}

	private void initialize() 
	{
		frame = new JFrame("TP inicial II - Agenda (ABM) - LCS - UNGS 02/2019 - Godoy");
		frame.setBounds(100, 100, 1000, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 974, 561);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		try
		{
			frame.setIconImage(ImageIO.read(getClass().getResourceAsStream("favicon.png")));
		} catch (IOException e){ e.printStackTrace(); }
		
		// seccion columnas de personas.
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 954, 206);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		
		spPersonas.setViewportView(tablaPersonas);
		
		// seccion columnas de localidades.
		JScrollPane spLocalidades = new JScrollPane();
		spLocalidades.setBounds(10, 302, 225, 146);
		panel.add(spLocalidades);
		
		modelLocalidades = new DefaultTableModel(null,localidadColumna);
		tablaLocalidades = new JTable(modelLocalidades);
		
		tablaLocalidades.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaLocalidades.getColumnModel().getColumn(0).setResizable(false);
		
		spLocalidades.setViewportView(tablaLocalidades);
		
		// seccion columnas de tipos de contacto.
		JScrollPane spTiposDeContacto = new JScrollPane();
		spTiposDeContacto.setBounds(721, 302, 243, 146);
		panel.add(spTiposDeContacto);
		
		modelTiposDeContacto = new DefaultTableModel(null,tiposDeContactoColumna);
		tablaTiposDeContacto = new JTable(modelTiposDeContacto);
		
		tablaTiposDeContacto.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaTiposDeContacto.getColumnModel().getColumn(0).setResizable(false);
		
		spTiposDeContacto.setViewportView(tablaTiposDeContacto);
		
		btnAgregarContacto = new JButton("Agregar contacto");
		btnAgregarContacto.setBounds(267, 228, 145, 23);
		panel.add(btnAgregarContacto);
		
		btnEditarContacto = new JButton("Editar contacto");
		btnEditarContacto.setBounds(422, 228, 135, 23);
		panel.add(btnEditarContacto);
		
		btnBorrarContacto = new JButton("Borrar contacto");
		btnBorrarContacto.setBounds(567, 228, 126, 23);
		panel.add(btnBorrarContacto);
		
		btnGenerarReporte = new JButton("Reporte");
		btnGenerarReporte.setBounds(862, 228, 89, 23);
		panel.add(btnGenerarReporte);
		
		btnAñadirLocalidad = new JButton("Añadir localidad");
		btnAñadirLocalidad.setBounds(54, 459, 126, 23);
		panel.add(btnAñadirLocalidad);
		
		btnEditarLocalidad = new JButton("Editar localidad");
		btnEditarLocalidad.setBounds(54, 493, 126, 23);
		panel.add(btnEditarLocalidad);
		
		btnBorrarLocalidad = new JButton("Borrar localidad");
		btnBorrarLocalidad.setBounds(54, 527, 126, 23);
		panel.add(btnBorrarLocalidad);
		
		btnAñadirTipoDeContacto = new JButton("Añadir tipo de contacto");
		btnAñadirTipoDeContacto.setBounds(762, 459, 175, 23);
		panel.add(btnAñadirTipoDeContacto);
		
		btnEditarTipoDeContacto = new JButton("Editar tipo de contacto");
		btnEditarTipoDeContacto.setBounds(762, 493, 175, 23);
		panel.add(btnEditarTipoDeContacto);
		
		btnBorrarTipoDeContacto = new JButton("Borrar tipo de contacto");
		btnBorrarTipoDeContacto.setBounds(762, 527, 175, 23);
		panel.add(btnBorrarTipoDeContacto);
		
		btnDbPrueba = new JButton("Conexión DB");
		btnDbPrueba.setBounds(383, 527, 126, 23);
		panel.add(btnDbPrueba);
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) 
			{
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		}
		);
		this.frame.setVisible(true);
	}
	
	public JButton getBtnAgregarContacto() 
	{
		return btnAgregarContacto;
	}

	public JButton getBtnEditarContacto()
	{
		return btnEditarContacto;
	}

	public JButton getBtnBorrarContacto() 
	{
		return btnBorrarContacto;
	}
	
	public JButton getBtnGenerarReporte() 
	{
		return btnGenerarReporte;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreContactoColumnas() 
	{
		return nombreColumnas;
	}
	
	public JTable getTablaLocalidades()
	{
		return tablaLocalidades;
	}

	public JTable getTablaTiposDeContacto()
	{
		return tablaTiposDeContacto;
	}

	public JButton getBtnAñadirLocalidad()
	{
		return btnAñadirLocalidad;
	}

	public JButton getBtnEditarLocalidad()
	{
		return btnEditarLocalidad;
	}

	public JButton getBtnBorrarLocalidad()
	{
		return btnBorrarLocalidad;
	}

	public JButton getBtnAñadirTipoDeContacto()
	{
		return btnAñadirTipoDeContacto;
	}

	public JButton getBtnEditarTipoDeContacto()
	{
		return btnEditarTipoDeContacto;
	}

	public JButton getBtnDbPrueba()
	{
		return btnDbPrueba;
	}

	public JButton getBtnBorrarTipoDeContacto()
	{
		return btnBorrarTipoDeContacto;
	}

	public DefaultTableModel getModelLocalidades()
	{
		return modelLocalidades;
	}

	public DefaultTableModel getModelTiposDeContacto()
	{
		return modelTiposDeContacto;
	}

	public String[] getNombreLocalidadColumna()
	{
		return localidadColumna;
	}

	public String[] getNombreTiposDeContactoColumna()
	{
		return tiposDeContactoColumna;
	}

	public void llenarTablaContactos(List<PersonaDTO> personasEnTabla) 
	{
		this.getModelPersonas().setRowCount(0);
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreContactoColumnas());

		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String calle = p.getCalle();
			String altura = p.getAltura();
			String piso = p.getPiso();
			String dpto = p.getDpto();
			String localidad = p.getLocalidad();
			String email = p.getEmail();
			Date fechaCumple = p.getFechaCumple();
			String tipoContacto = p.getTipoContacto();
			String tel = p.getTelefono();
			
			Object[] fila = {nombre, calle, altura, piso, dpto, localidad, email, fechaCumple, tipoContacto, tel};
			this.getModelPersonas().addRow(fila);
		}
	}
	
	public void llenarTablaLocalidades(List<LocalidadDTO> localidadesEnTabla)
	{
		this.getModelLocalidades().setRowCount(0);
		this.getModelLocalidades().setColumnCount(0);
		this.getModelLocalidades().setColumnIdentifiers(this.getNombreLocalidadColumna());

		for (LocalidadDTO l : localidadesEnTabla)
		{
			String descripcion = l.getDescripcion();
			
			Object[] fila = {descripcion};
			this.getModelLocalidades().addRow(fila);
		}
	}
	
	public void llenarTablaTiposDeContacto(List<TipoContactoDTO> tiposDeContactoEnTabla)
	{
		this.getModelTiposDeContacto().setRowCount(0);
		this.getModelTiposDeContacto().setColumnCount(0);
		this.getModelTiposDeContacto().setColumnIdentifiers(this.getNombreTiposDeContactoColumna());

		for (TipoContactoDTO t : tiposDeContactoEnTabla)
		{
			String concepto = t.getConcepto();
			
			Object[] fila = {concepto};
			this.getModelTiposDeContacto().addRow(fila);
		}
	}
	
	public void errorSinPersonasParaReporte()
	{
	    String message = "No es posible generar un reporte sin personas en la agenda.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Cuidado!", JOptionPane.WARNING_MESSAGE);
	}
	public void faltaSeleccionItemError()
	{
	    String message = "¡Error! Falta seleccionar un ítem válido.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Cuidado!", JOptionPane.ERROR_MESSAGE);
	}

	public void errorImposibleEliminarItem()
	{
	    String message = "¡Error! No es posible eliminar este ítem.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Cuidado!", JOptionPane.ERROR_MESSAGE);
	}
}