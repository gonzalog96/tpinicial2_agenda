package presentacion.vista;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaEdicionTipoDeContacto extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtNombreTipoDeContacto;
	
	private JButton btnEditarTipoDeContacto;
	
	private static VentanaEdicionTipoDeContacto INSTANCE;
	
	public static VentanaEdicionTipoDeContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaEdicionTipoDeContacto(); 	
			return new VentanaEdicionTipoDeContacto();
		}
		else
			return INSTANCE;
	}

	private VentanaEdicionTipoDeContacto() 
	{
		super();
		try
		{
			setIconImage(ImageIO.read(getClass().getResourceAsStream("favicon.png")));
			setTitle("TP inicial II - Agenda (ABM) - LCS - UNGS 02/2019 - Godoy");
		} catch (IOException e){ e.printStackTrace(); }
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 360, 148);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 334, 98);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreTipoDeContacto = new JLabel("Nuevo nombre del tipo de contacto: ");
		lblNombreTipoDeContacto.setBounds(0, 11, 215, 14);
		panel.add(lblNombreTipoDeContacto);
		
		txtNombreTipoDeContacto = new JTextField();
		txtNombreTipoDeContacto.setBounds(210, 8, 119, 20);
		panel.add(txtNombreTipoDeContacto);
		txtNombreTipoDeContacto.setColumns(10);
		
		btnEditarTipoDeContacto = new JButton("Guardar datos");
		btnEditarTipoDeContacto.setBounds(74, 48, 174, 28);
		panel.add(btnEditarTipoDeContacto);
		
		this.setVisible(false);
	}
	
	public void precargarDatos(String nombreActual) 
	{
		this.txtNombreTipoDeContacto.setText(nombreActual);
	}
	
	public void faltaDescripcionTipoDeContacto()
	{
	    String message = "¡Error al actualizar la información!\n El nombre del tipo de contacto no puede ser nulo/vacío.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	public void tipoDeContactoRepetido()
	{
		String message = "El nombre del tipo de contacto ya existe en el sistema.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	
	public void mostrar()
	{
		this.setVisible(true);
	}
	
	public JTextField getNombreTipoDeContacto() 
	{
		return txtNombreTipoDeContacto;
	}

	public JButton getBtnEditarTipoDeContacto() 
	{
		return btnEditarTipoDeContacto;
	}

	public void cerrar()
	{
		this.txtNombreTipoDeContacto.setText(null);
		this.dispose();
	}
}