package presentacion.vista;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import dto.LocalidadDTO;
import dto.TipoContactoDTO;

import java.awt.Color;
import java.io.IOException;

import javax.swing.JComboBox;

public class VentanaNuevoContacto extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtNombre;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDpto;
	private JTextField txtEmail;
	private JTextField txtTelefono;
	
	private JComboBox<String> localidad;
	private JComboBox<String> tipoContacto;
	private JDateChooser fechaCumple;
	
	private JButton btnAgregarPersona;
	
	private static VentanaNuevoContacto INSTANCE;
	
	public static VentanaNuevoContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaNuevoContacto(); 	
			return new VentanaNuevoContacto();
		}
		else
			return INSTANCE;
	}

	private VentanaNuevoContacto() 
	{
		super();
		try
		{
			setIconImage(ImageIO.read(getClass().getResourceAsStream("favicon.png")));
			setTitle("TP inicial II - Agenda (ABM) - LCS - UNGS 02/2019 - Godoy");
		} catch (IOException e){ e.printStackTrace(); }
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 378);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 404, 337);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 11, 113, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 36, 113, 14);
		panel.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 61, 113, 14);
		panel.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(10, 86, 113, 14);
		panel.add(lblPiso);
		
		JLabel lblDpto = new JLabel("Dpto");
		lblDpto.setBounds(10, 111, 113, 14);
		panel.add(lblDpto);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 136, 113, 14);
		panel.add(lblLocalidad);

		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setBounds(10, 171, 113, 14);
		panel.add(lblEmail);
		
		JLabel lblFechaCumple = new JLabel("Fecha de cumpleaños");
		lblFechaCumple.setBounds(10, 207, 131, 14);
		panel.add(lblFechaCumple);
		
		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setBounds(10, 232, 113, 14);
		panel.add(lblTelefono);
		
		JLabel lblTipoContacto = new JLabel("Tipo de contacto");
		lblTipoContacto.setBounds(10, 257, 113, 14);
		panel.add(lblTipoContacto);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(165, 8, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtCalle = new JTextField();
		txtCalle.setBounds(165, 33, 164, 20);
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		txtAltura = new JTextField();
		txtAltura.setBounds(165, 58, 164, 20);
		panel.add(txtAltura);
		txtAltura.setColumns(10);
		
		txtPiso = new JTextField();
		txtPiso.setBounds(165, 83, 164, 20);
		panel.add(txtPiso);
		txtPiso.setColumns(10);
		
		txtDpto = new JTextField();
		txtDpto.setBounds(165, 108, 164, 20);
		panel.add(txtDpto);
		txtDpto.setColumns(10);
		
		localidad = new JComboBox<String>();
		localidad.setBounds(165, 133, 164, 20);
		panel.add(localidad);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(165, 168, 164, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		fechaCumple = new JDateChooser();
		fechaCumple.setBackground(Color.RED);
		fechaCumple.setLocale(new Locale("es"));
		fechaCumple.setDateFormatString("yyyy-MM-dd");
		fechaCumple.getJCalendar().setMaxSelectableDate(new Date());
		fechaCumple.setBounds(165, 201, 164, 20);
		
		JTextFieldDateEditor editor = (JTextFieldDateEditor) fechaCumple.getDateEditor();
		editor.setEditable(false);
		
		panel.add(fechaCumple);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(165, 229, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		tipoContacto = new JComboBox<String>();
		tipoContacto.setBounds(165, 254, 146, 20);
		panel.add(tipoContacto);
		
		btnAgregarPersona = new JButton("Añadir contacto");
		btnAgregarPersona.setBounds(138, 285, 138, 28);
		panel.add(btnAgregarPersona);
		
		this.setVisible(false);
	}
	
	public void cargarLocalidades(List<LocalidadDTO> localidades)
	{
		this.localidad.removeAllItems();
		for(LocalidadDTO l : localidades) 
		{
			this.localidad.addItem(l.getDescripcion());
		}
	}
	
	public void cargarTiposDeContacto(List<TipoContactoDTO> tipoDeContactos)
	{
		this.tipoContacto.removeAllItems();
		for(TipoContactoDTO t : tipoDeContactos) 
		{
			this.tipoContacto.addItem(t.getConcepto());
		}
	}
	
	public void faltanDatosError()
	{
	    String message = "¡Error al añadir el contacto!\n Verificar que los siguientes campos estén completos:\n- Nombre y apellido.\n- Calle.\n- Altura.\n- Localidad.\n- Fecha de cumpleaños.\n- Tipo de contacto.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	
	public void contactoExistenteError()
	{
	    String message = "El contacto ya existe en la base de datos.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Error al añadir!", JOptionPane.ERROR_MESSAGE);
	}
	
	public void mostrarVentana()
	{
		this.txtNombre.setText(null);
		this.txtAltura.setText(null);
		this.txtCalle.setText(null);
		this.txtDpto.setText(null);
		this.txtEmail.setText(null);
		this.txtPiso.setText(null);
		this.txtTelefono.setText(null);
		this.fechaCumple.setDate(null);
		
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtCalle()
	{
		return txtCalle;
	}

	public JTextField getTxtAltura()
	{
		return txtAltura;
	}

	public JTextField getTxtPiso()
	{
		return txtPiso;
	}

	public JTextField getTxtDpto()
	{
		return txtDpto;
	}

	public JTextField getTxtEmail()
	{
		return txtEmail;
	}

	public String getTxtLocalidad()
	{
		return (String) localidad.getSelectedItem();
	}

	public String getTxtTipoContacto()
	{
		return (String) tipoContacto.getSelectedItem();
	}

	public Date getTxtCumple()
	{
		return fechaCumple.getDate();
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}

	public JButton getBtnAgregarPersona() 
	{
		return btnAgregarPersona;
	}

	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.dispose();
	}
}