package presentacion.vista;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaEdicionLocalidad extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtNombreLocalidad;
	
	private JButton btnEditarLocalidad;
	
	private static VentanaEdicionLocalidad INSTANCE;
	
	public static VentanaEdicionLocalidad getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaEdicionLocalidad(); 	
			return new VentanaEdicionLocalidad();
		}
		else
			return INSTANCE;
	}

	private VentanaEdicionLocalidad() 
	{
		super();
		try
		{
			setIconImage(ImageIO.read(getClass().getResourceAsStream("favicon.png")));
			setTitle("TP inicial II - Agenda (ABM) - LCS - UNGS 02/2019 - Godoy");
		} catch (IOException e){ e.printStackTrace(); }
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 360, 148);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 334, 98);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreLocalidad = new JLabel("Nuevo nombre de la localidad: ");
		lblNombreLocalidad.setBounds(10, 11, 172, 14);
		panel.add(lblNombreLocalidad);
		
		txtNombreLocalidad = new JTextField();
		txtNombreLocalidad.setBounds(192, 8, 137, 20);
		panel.add(txtNombreLocalidad);
		txtNombreLocalidad.setColumns(10);
		
		btnEditarLocalidad = new JButton("Guardar datos");
		btnEditarLocalidad.setBounds(81, 47, 174, 28);
		panel.add(btnEditarLocalidad);
		
		this.setVisible(false);
	}
	
	public void precargarDatos(String nombreActual) 
	{
		this.txtNombreLocalidad.setText(nombreActual);
	}
	
	public void faltaDescripcion()
	{
	    String message = "¡Error al actualizar la información!\n El nombre de la localidad no puede ser nulo/vacío.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	public void tipoDeLocalidadRepetida()
	{
		String message = "El nombre de la localidad ya existe en el sistema.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	
	public void mostrar()
	{
		this.setVisible(true);
	}
	
	public JTextField getNombreLocalidad() 
	{
		return txtNombreLocalidad;
	}

	public JButton getBtnEditarLocalidad() 
	{
		return btnEditarLocalidad;
	}

	public void cerrar()
	{
		this.txtNombreLocalidad.setText(null);
		this.dispose();
	}
}