package presentacion.vista;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaNuevoTipoDeContacto extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtNombreTipoDeContacto;
	
	private JButton btnAgregarTipoDeContacto;
	
	private static VentanaNuevoTipoDeContacto INSTANCE;
	
	public static VentanaNuevoTipoDeContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaNuevoTipoDeContacto(); 	
			return new VentanaNuevoTipoDeContacto();
		}
		else
			return INSTANCE;
	}
	
	private VentanaNuevoTipoDeContacto() 
	{
		super();
		try
		{
			setIconImage(ImageIO.read(getClass().getResourceAsStream("favicon.png")));
			setTitle("TP inicial II - Agenda (ABM) - LCS - UNGS 02/2019 - Godoy");
		} catch (IOException e){ e.printStackTrace(); }
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 740, 539);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreTipoDeContacto = new JLabel("Nombre del tipo de contacto: ");
		lblNombreTipoDeContacto.setBounds(10, 11, 165, 14);
		panel.add(lblNombreTipoDeContacto);
		
		txtNombreTipoDeContacto = new JTextField();
		txtNombreTipoDeContacto.setBounds(185, 8, 164, 20);
		panel.add(txtNombreTipoDeContacto);
		txtNombreTipoDeContacto.setColumns(10);
		
		btnAgregarTipoDeContacto = new JButton("Añadir tipo de contacto");
		btnAgregarTipoDeContacto.setBounds(554, 500, 176, 28);
		panel.add(btnAgregarTipoDeContacto);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.txtNombreTipoDeContacto.setText(null);
		this.setVisible(true);
	}
	
	public void faltanDatosError()
	{
	    String message = "¡Error! El nombre del tipo de contacto no puede ser nulo/vacío.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	public void tipoDeContactoRepetido()
	{
		String message = "El tipo de contacto ya existe en el sistema.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Atención!", JOptionPane.ERROR_MESSAGE);
	}
	
	public JTextField getTipoDeContacto() 
	{
		return txtNombreTipoDeContacto;
	}
	
	public JButton getBtnTipoDeContacto() 
	{
		return btnAgregarTipoDeContacto;
	}
	
	public void cerrar()
	{
		this.txtNombreTipoDeContacto.setText(null);
		this.dispose();
	}
}