package presentacion.vista;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionEvent;

public class VentanaConexionDB extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtDireccion;
	private JTextField txtPuerto;
	private JTextField txtUsuario;
	private JTextField txtContrasenia;
	
	private JButton btnProbarConexion;
	private JButton btnGuardarYConectar;
	private JButton btnLimpiar;
	
	private static VentanaConexionDB INSTANCE;
	
	public static VentanaConexionDB getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new VentanaConexionDB(); 	
			return new VentanaConexionDB();
		}
		else
			return INSTANCE;
	}
	
	private VentanaConexionDB() 
	{
		try
		{
			setIconImage(ImageIO.read(getClass().getResourceAsStream("favicon.png")));
			setTitle("TP inicial II - Agenda (ABM) - LCS - UNGS 02/2019 - Godoy");
		} catch (IOException e){ e.printStackTrace(); }
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 414, 239);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreLocalidad = new JLabel("Dirección de la DB: ");
		lblNombreLocalidad.setBounds(10, 11, 127, 14);
		panel.add(lblNombreLocalidad);
		
		txtDireccion = new JTextField();
		txtDireccion.setToolTipText("localhost");
		txtDireccion.setBounds(147, 8, 164, 20);
		panel.add(txtDireccion);
		txtDireccion.setColumns(10);
		
		btnProbarConexion = new JButton("Añadir localidad");
		btnProbarConexion.setBounds(554, 500, 176, 28);
		panel.add(btnProbarConexion);
		
		JLabel lblPuertoDeLaDB = new JLabel("Puerto de la DB: ");
		lblPuertoDeLaDB.setBounds(10, 53, 123, 14);
		panel.add(lblPuertoDeLaDB);
		
		txtPuerto = new JTextField();
		txtPuerto.setToolTipText("5432");
		txtPuerto.setColumns(10);
		txtPuerto.setBounds(147, 50, 164, 20);
		panel.add(txtPuerto);
		
		JLabel lblUsuario = new JLabel("Usuario: ");
		lblUsuario.setBounds(10, 92, 123, 14);
		panel.add(lblUsuario);
		
		txtUsuario = new JTextField();
		txtUsuario.setToolTipText("postgres");
		txtUsuario.setColumns(10);
		txtUsuario.setBounds(147, 89, 164, 20);
		panel.add(txtUsuario);
		
		JLabel lblContrasenia = new JLabel("Contraseña: ");
		lblContrasenia.setBounds(10, 127, 123, 14);
		panel.add(lblContrasenia);
		
		txtContrasenia = new JTextField();
		txtContrasenia.setToolTipText("Armado01");
		txtContrasenia.setColumns(10);
		txtContrasenia.setBounds(147, 124, 164, 20);
		panel.add(txtContrasenia);
		
		btnProbarConexion = new JButton("Probar conexión");
		btnProbarConexion.setBounds(0, 216, 133, 23);
		panel.add(btnProbarConexion);
		
		btnGuardarYConectar = new JButton("Guardar y conectar");
		btnGuardarYConectar.setBounds(143, 216, 144, 23);
		panel.add(btnGuardarYConectar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(e->limpiarDatos(e));
		
		btnLimpiar.setBounds(309, 216, 95, 23);
		panel.add(btnLimpiar);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public void precargarDatos(String direccion, String puerto, String usuario, String contrasenia)
	{
		this.txtDireccion.setText(direccion);
		this.txtContrasenia.setText(contrasenia);
		this.txtPuerto.setText(puerto);
		this.txtUsuario.setText(usuario);
	}
	
	public void faltanDatosError()
	{
	    String message = "Se deben completar todos los datos del formulario (dirección, puerto, usuario y contraseña).";
	    JOptionPane.showMessageDialog(new JFrame(), message, "¡Error!", JOptionPane.WARNING_MESSAGE);
	}
	public void ocurrioUnProblemaBD()
	{
		String message = "Ocurrió un problema al guardar los datos en la DB. Intentá más tarde.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "Error al guardar los datos - JSON", JOptionPane.ERROR_MESSAGE);
	}
	public void conexionFallida()
	{
		String message = "No se pudo conectar a la DB. Se deben revisar los parámetros de conexión (dirección, puerto, usuario y contraseña).";
	    JOptionPane.showMessageDialog(new JFrame(), message, "Error en la conexión", JOptionPane.ERROR_MESSAGE);
	}
	public void conexionExitosa()
	{
		String message = "¡Ok! Se pudo conectar sin problemas a la DB.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "Conexión efectuada", JOptionPane.INFORMATION_MESSAGE);
	}
	public void todoListo()
	{
		String message = "¡Todo listo! Esta ventana se cerrará para guardar los cambios.\n Por favor, luego del cierre, abrí nuevamente la agenda desde el acceso directo.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "Reiniciar para guardar cambios", JOptionPane.WARNING_MESSAGE);
	    //System.exit(0);
	}
	public void ventanaDatosGuardados()
	{
		String message = "¡Todo listo! Los datos han sido guardados.";
	    JOptionPane.showMessageDialog(new JFrame(), message, "Datos guardados", JOptionPane.WARNING_MESSAGE);
	}
	public void limpiarDatos(ActionEvent e)
	{
		this.txtDireccion.setText(null);
		this.txtContrasenia.setText(null);
		this.txtPuerto.setText(null);
		this.txtUsuario.setText(null);	
	}
	public void cerrar()
	{
		this.txtDireccion.setText(null);
		this.txtContrasenia.setText(null);
		this.txtPuerto.setText(null);
		this.txtUsuario.setText(null);
		
		this.dispose();
	}

	public JTextField getTxtDireccion()
	{
		return txtDireccion;
	}

	public JTextField getTxtPuerto()
	{
		return txtPuerto;
	}

	public JTextField getTxtUsuario()
	{
		return txtUsuario;
	}

	public JTextField getTxtContrasenia()
	{
		return txtContrasenia;
	}

	public JButton getBtnProbarConexion()
	{
		return btnProbarConexion;
	}

	public JButton getBtnGuardarYConectar()
	{
		return btnGuardarYConectar;
	}

	public JButton getBtnLimpiar()
	{
		return btnLimpiar;
	}
}