package dto;

public class TipoContactoDTO
{
	private int idTipoContacto;
	private String concepto;
	
	public TipoContactoDTO(int idTipoContacto, String concepto)
	{
		this.idTipoContacto = idTipoContacto;
		this.concepto = concepto;
	}
	
	public int getIdTipoContacto()
	{
		return idTipoContacto;
	}
	public void setIdTipoContacto(int idTipoContacto)
	{
		this.idTipoContacto = idTipoContacto;
	}
	public String getConcepto()
	{
		return concepto;
	}
	public void setConcepto(String concepto)
	{
		this.concepto = concepto;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((concepto == null) ? 0 : concepto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		TipoContactoDTO other = (TipoContactoDTO) obj;
		if (concepto == null)
		{
			if (other.concepto != null)
				return false;
		} 
		else if (!concepto.equals(other.concepto))
			return false;
		
		return true;
	}
}