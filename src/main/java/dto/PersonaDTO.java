package dto;

import java.util.Date;

public class PersonaDTO
{
	private int idPersona;
	
	private String nombre;
	private String calle;
	private String altura;
	private String piso;
	private String dpto;
	private String localidad;
	private String email;
	private Date fechaCumple;
	private String tipoContacto;
	private String telefono;

	public PersonaDTO(int idPersona, String nombre, String calle, String altura, String piso, String dpto, String localidad, String email, Date fechaCumple, String tipoContacto, String telefono)
	{
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.calle = calle;
		this.altura = altura;
		this.piso = piso;
		this.dpto = dpto;
		this.localidad = localidad;
		this.email = email;
		this.fechaCumple = fechaCumple;
		this.tipoContacto = tipoContacto;
		this.telefono = telefono;
	}
	
	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getCalle()
	{
		return calle;
	}

	public void setCalle(String calle)
	{
		this.calle = calle;
	}

	public String getAltura()
	{
		return altura;
	}

	public void setAltura(String altura)
	{
		this.altura = altura;
	}

	public String getPiso()
	{
		return piso;
	}

	public void setPiso(String piso)
	{
		this.piso = piso;
	}

	public String getDpto()
	{
		return dpto;
	}

	public void setDpto(String dpto)
	{
		this.dpto = dpto;
	}

	public String getLocalidad()
	{
		return localidad;
	}

	public void setLocalidad(String localidad)
	{
		this.localidad = localidad;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public Date getFechaCumple()
	{
		return fechaCumple;
	}

	public void setFechaCumple(Date fechaCumple)
	{
		this.fechaCumple = fechaCumple;
	}

	public String getTipoContacto()
	{
		return tipoContacto;
	}

	public void setTipoContacto(String tipoContacto)
	{
		this.tipoContacto = tipoContacto;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}
	
	

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((altura == null) ? 0 : altura.hashCode());
		result = prime * result + ((calle == null) ? 0 : calle.hashCode());
		result = prime * result + ((dpto == null) ? 0 : dpto.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((fechaCumple == null) ? 0 : fechaCumple.hashCode());
		result = prime * result + ((localidad == null) ? 0 : localidad.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((piso == null) ? 0 : piso.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		result = prime * result + ((tipoContacto == null) ? 0 : tipoContacto.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		PersonaDTO other = (PersonaDTO) obj;
		if (altura == null)
		{
			if (other.altura != null)
				return false;
		} 
		else if (!altura.equals(other.altura))
			return false;
		
		if (calle == null)
		{
			if (other.calle != null)
				return false;
		} 
		else if (!calle.equals(other.calle))
			return false;
		
		if (dpto == null)
		{
			if (other.dpto != null)
				return false;
		} 
		else if (!dpto.equals(other.dpto))
			return false;
		
		if (email == null)
		{
			if (other.email != null)
				return false;
		} 
		else if (!email.equals(other.email))
			return false;
		
		if (fechaCumple == null)
		{
			if (other.fechaCumple != null)
				return false;
		} 
		else if (!fechaCumple.equals(other.fechaCumple))
			return false;
		
		if (localidad == null)
		{
			if (other.localidad != null)
				return false;
		} 
		else if (!localidad.equals(other.localidad))
			return false;
		
		if (nombre == null)
		{
			if (other.nombre != null)
				return false;
		} 
		else if (!nombre.equals(other.nombre))
			return false;
		
		if (piso == null)
		{
			if (other.piso != null)
				return false;
		} 
		else if (!piso.equals(other.piso))
			return false;
		
		if (telefono == null)
		{
			if (other.telefono != null)
				return false;
		} 
		else if (!telefono.equals(other.telefono))
			return false;
		
		if (tipoContacto == null)
		{
			if (other.tipoContacto != null)
				return false;
		} 
		else if (!tipoContacto.equals(other.tipoContacto))
			return false;
		
		return true;
	}
}