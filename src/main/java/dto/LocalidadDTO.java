package dto;

public class LocalidadDTO
{
	private int idLocalidad;
	private String descripcion;
	
	public LocalidadDTO(int idLocalidad, String descripcion)
	{
		this.idLocalidad = idLocalidad;
		this.descripcion = descripcion;
	}

	public int getIdLocalidad()
	{
		return idLocalidad;
	}

	public void setIdLocalidad(int idLocalidad)
	{
		this.idLocalidad = idLocalidad;
	}

	public String getDescripcion()
	{
		return descripcion;
	}

	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		LocalidadDTO other = (LocalidadDTO) obj;
		if (descripcion == null)
		{
			if (other.descripcion != null)
				return false;
			
		} 
		else if (!descripcion.equals(other.descripcion))
			return false;
		
		return true;
	}
}