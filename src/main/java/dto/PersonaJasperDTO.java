package dto;

public class PersonaJasperDTO
{
	private String nombre;
	private String calle;
	private String altura;
	private String piso;
	private String dpto;
	private String localidad;
	private String email;
	private String fechaCumple;
	
	// magia
	private String anioCumple;
	
	private String tipoContacto;
	private String telefono;
	
	public PersonaJasperDTO(String nombre, String calle, String altura, String piso, String dpto, String localidad, String email, String fechaCumple, String anioCumple, String tipoContacto, String telefono)
	{
		this.nombre = nombre;
		this.calle = calle;
		this.altura = altura;
		this.piso = piso;
		this.dpto = dpto;
		this.localidad = localidad;
		this.email = email;
		this.fechaCumple = fechaCumple;
		this.anioCumple = anioCumple;
		this.tipoContacto = tipoContacto;
		this.telefono = telefono;
	}

	public String getNombre()
	{
		return nombre;
	}

	public String getCalle()
	{
		return calle;
	}

	public String getAltura()
	{
		return altura;
	}

	public String getPiso()
	{
		return piso;
	}

	public String getDpto()
	{
		return dpto;
	}

	public String getLocalidad()
	{
		return localidad;
	}

	public String getEmail()
	{
		return email;
	}

	public String getFechaCumple()
	{
		return fechaCumple;
	}

	public String getAnioCumple()
	{
		return anioCumple;
	}

	public String getTipoContacto()
	{
		return tipoContacto;
	}

	public String getTelefono()
	{
		return telefono;
	}
}