package dto;

public class ConexionDBDTO
{
	private String direccion;
	private String puerto;
	private String usuario;
	private String contrasenia;
	
	public ConexionDBDTO(String direccion, String puerto, String usuario, String contrasenia)
	{
		this.direccion = direccion;
		this.puerto = puerto;
		this.usuario = usuario;
		this.contrasenia = contrasenia;
	}

	public String getDireccion()
	{
		return direccion;
	}

	public String getPuerto()
	{
		return puerto;
	}

	public String getUsuario()
	{
		return usuario;
	}

	public String getContrasenia()
	{
		return contrasenia;
	}
}