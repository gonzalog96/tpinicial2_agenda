package persistencia.dao.mysql;

import java.sql.Connection;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, nombre, calle, altura, piso, dpto, localidad, email, fechaCumple, tipoContacto, telefono) VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM personas WHERE idPersona::text = ?";
	
	private static final String updateLocalidad = "UPDATE personas SET localidad = ? WHERE idPersona::text = ?";
	private static final String updateTipoDeContacto = "UPDATE personas SET tipocontacto = ? WHERE idPersona::text = ?";
	
	private static final String readall = "SELECT * FROM personas";
		
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);

			statement.setString(1, persona.getNombre());
			statement.setString(2, persona.getCalle());
			statement.setString(3, persona.getAltura());
			statement.setString(4, persona.getPiso());
			statement.setString(5, persona.getDpto());
			statement.setString(6, persona.getLocalidad());
			statement.setString(7, persona.getEmail());
			
			java.sql.Date sqlStartDate = new java.sql.Date(persona.getFechaCumple().getTime());
			statement.setDate(8, sqlStartDate);

			statement.setString(9, persona.getTipoContacto());
			statement.setString(10, persona.getTelefono());
			
			if (statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean updateLocalidad(PersonaDTO persona, String nuevaLocalidad)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateLocalidadExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(updateLocalidad);
			statement.setString(1, nuevaLocalidad);
			statement.setString(2, Integer.toString(persona.getIdPersona()));
			if (statement.executeUpdate() > 0)
			{
				conexion.commit();
				isUpdateLocalidadExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isUpdateLocalidadExitoso;
	}
	
	public boolean updateTipoDeContacto(PersonaDTO persona, String tipoDeContacto)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateTipoDeContactoExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(updateTipoDeContacto);
			statement.setString(1, tipoDeContacto);
			statement.setString(2, Integer.toString(persona.getIdPersona()));
			if (statement.executeUpdate() > 0)
			{
				conexion.commit();
				isUpdateTipoDeContactoExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isUpdateTipoDeContactoExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if (statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall); //readll
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
/*		
		// ordenamos a las personas por su tipo de contacto.
		Collections.sort(personas, new Comparator<PersonaDTO>() {
		    public int compare(PersonaDTO p1, PersonaDTO p2) {
		        return p1.getTipoContacto().compareTo(p2.getTipoContacto());
		    }
		});
		*/
		return personas;
	}
/*	public List<PersonaDTO> readAllOrderedByTipoContacto()
	{
		PreparedStatement statement;
		ResultSet resultSet;
		
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(read_all_ordered_by_tipo_contacto);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}*/
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("Nombre");
		
		String calle = resultSet.getString("Calle");
		String altura = resultSet.getString("Altura");
		String piso = resultSet.getString("Piso");
		String dpto = resultSet.getString("Dpto");
		String localidad = resultSet.getString("Localidad");
		String email = resultSet.getString("Email");
		Date fechaCumple = resultSet.getDate("fechaCumple");
		String tipoContacto = resultSet.getString("tipoContacto");
		
		String tel = resultSet.getString("Telefono");
		
		return new PersonaDTO(id, nombre, calle, altura, piso, dpto, localidad, email, fechaCumple, tipoContacto, tel);
	}
}