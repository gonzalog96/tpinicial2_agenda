package persistencia.dao.mysql;

import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoDeContactoDAO;

public class DAOSQLFactory implements DAOAbstractFactory 
{
	public PersonaDAO createPersonaDAO() 
	{
	   return new PersonaDAOSQL();
	}

	public LocalidadDAO createLocalidadDAO()
	{
		return new LocalidadDAOSQL();
	}

	public TipoDeContactoDAO createTipoContactoDAO()
	{
		return new TipoContactoDAOSQL();
	}
}