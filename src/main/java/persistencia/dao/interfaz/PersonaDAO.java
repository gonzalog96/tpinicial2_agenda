package persistencia.dao.interfaz;

import java.util.List;

import dto.PersonaDTO;

public interface PersonaDAO 
{
	public boolean insert(PersonaDTO persona);
	public boolean delete(PersonaDTO persona_a_eliminar);
	
	public boolean updateLocalidad(PersonaDTO persona, String nuevaLocalidad);
	public boolean updateTipoDeContacto(PersonaDTO persona, String tipoDeContacto);
	
	public List<PersonaDTO> readAll();
	//public List<PersonaDTO> readAllOrderedByTipoContacto();
}