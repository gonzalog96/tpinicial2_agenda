package persistencia.dao.interfaz;

import java.util.List;

import dto.TipoContactoDTO;

public interface TipoDeContactoDAO
{
	public boolean insert(TipoContactoDTO tipoDeContacto);
	public boolean delete(TipoContactoDTO tipoDeContacto_a_eliminar);
	public List<TipoContactoDTO> readAll();
}
