package persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;

import dto.ConexionDBDTO;

public class Conexion
{
	public static Conexion instancia;
	private Connection connection;
	private Logger log = Logger.getLogger(Conexion.class);	
	
	private Conexion(ConexionDBDTO conexion)
	{
		try
		{
			Class.forName("org.postgresql.Driver");
			
			this.connection = DriverManager.getConnection("jdbc:postgresql://" + conexion.getDireccion() + ":" + conexion.getPuerto() + "/grupo_5", conexion.getUsuario(), conexion.getContrasenia());
//			this.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/grupo_5", "postgres", "Armado01");
			this.connection.setAutoCommit(false);
			log.info("Conexión exitosa");
		}
		catch(Exception e)
		{
			log.error("Conexión fallida", e);
		}
	}
	
	public static Conexion getConexion()   
	{								
		if (instancia == null)
		{
			instancia = new Conexion(PersistirJSON.getInstance().obtenerDatos());
		}
		return instancia;
	}
	
	public static boolean esPosibleConectar(ConexionDBDTO conexion)
	{
		try {
			Class.forName("org.postgresql.Driver");
			
			@SuppressWarnings("unused")
			Connection connect = DriverManager.getConnection("jdbc:postgresql://" + conexion.getDireccion() + ":" + conexion.getPuerto() + "/grupo_5", conexion.getUsuario(), conexion.getContrasenia());
			return true;
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		return false;
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			log.info("Conexion cerrada");
		}
		catch (SQLException e) 
		{
			log.error("Error al cerrar la conexión!", e);
		}
		instancia = null;
	}
}