package persistencia.conexion;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import dto.ConexionDBDTO;

public class PersistirJSON
{
	private static PersistirJSON INSTANCE;
	
	public static PersistirJSON getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new PersistirJSON(); 	
			
			return new PersistirJSON();
		}
		else
			return INSTANCE;
	}
	
	@SuppressWarnings("unchecked")
	public boolean guardarEnDB(String direccion, String puerto, String usuario, String contrasenia)
	{
		JSONObject infoDB = new JSONObject();
		infoDB.put("direccion", direccion);
		infoDB.put("puerto", puerto);
		infoDB.put("usuario", usuario);
		infoDB.put("contrasenia", contrasenia);
		
		// escribimos al .json
        try (FileWriter file = new FileWriter("C:" + File.separator + "PostgreSQL10" + File.separator + "bin" + File.separator + "db_connection.json")) {
        	 
            file.write(infoDB.toJSONString());
            file.flush();
            
            return true;
 
        } catch (IOException e) 
        {
            e.printStackTrace();
        }
        
        return false;
	}
	
	public boolean existeArchivoJSON()
	{
		int length = 0;
		
		try 
		{
			JSONParser jsonParser = new JSONParser();
			Object obj = jsonParser.parse(new FileReader("C:" + File.separator + "PostgreSQL10" + File.separator + "bin" + File.separator + "db_connection.json"));
			JSONObject jsonObject = (JSONObject) obj;
			
			length = jsonObject.size();
			
		}
		catch (Exception e) {  }
		
		return (length > 0);
	}
	
	public ConexionDBDTO obtenerDatos()
	{
		JSONParser jsonParser = new JSONParser();
		try
		{
			Object obj = jsonParser.parse(new FileReader("C:" + File.separator + "PostgreSQL10" + File.separator + "bin" + File.separator + "db_connection.json"));
			JSONObject jsonObject = (JSONObject) obj;
			
			String direccion = (String) jsonObject.get("direccion");
			String puerto = (String) jsonObject.get("puerto");
			String usuario = (String) jsonObject.get("usuario");
			String contrasenia = (String) jsonObject.get("contrasenia");
			
			return new ConexionDBDTO(direccion, puerto, usuario, contrasenia);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public ConexionDBDTO obtenerDatosMock()
	{
		return new ConexionDBDTO("localhost", "5432", "postgres", "Armado01");
	}
}