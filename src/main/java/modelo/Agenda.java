package modelo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.PersonaJasperDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoDeContactoDAO;

public class Agenda 
{
	private PersonaDAO persona;	
	private LocalidadDAO localidad;
	private TipoDeContactoDAO tipoDeContacto;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.tipoDeContacto = metodo_persistencia.createTipoContactoDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}
	public void agregarNuevaLocalidad(LocalidadDTO nuevaLocalidad)
	{
		this.localidad.insert(nuevaLocalidad);
	}
	public void agregarNuevoTipoDeContacto(TipoContactoDTO nuevoTipoDeContacto)
	{
		this.tipoDeContacto.insert(nuevoTipoDeContacto);
	}
	
	public void actualizarDatosLocalidad(String localidadEliminada)
	{
		List<PersonaDTO> listadoPersonas = this.persona.readAll();
		for (PersonaDTO persona : listadoPersonas)
		{
			if (persona.getLocalidad().equals(localidadEliminada))
			{
				persona.setLocalidad("Ninguna");
				this.persona.updateLocalidad(persona, "Ninguna");
			}
		}
	}
	public void actualizarDatosLocalidad(String viejaLocalidad, String nuevaLocalidad)
	{
		List<PersonaDTO> listadoPersonas = this.persona.readAll();
		for (PersonaDTO persona : listadoPersonas)
		{
			if (persona.getLocalidad().equals(viejaLocalidad))
			{
				persona.setLocalidad(nuevaLocalidad);
				this.persona.updateLocalidad(persona, nuevaLocalidad);
			}
		}
	}
	
	public void actualizarDatosTipoDeContacto(String tipoDeContactoEliminado)
	{
		List<PersonaDTO> listadoPersonas = this.persona.readAll();
		for (PersonaDTO persona : listadoPersonas)
		{
			if (persona.getTipoContacto().equals(tipoDeContactoEliminado))
			{
				persona.setTipoContacto("Ninguno");
				this.persona.updateTipoDeContacto(persona, "Ninguno");
			}
		}
	}
	public void actualizarDatosTipoDeContacto(String viejoTipoDeContacto, String nuevoTipoDeContacto)
	{
		List<PersonaDTO> listadoPersonas = this.persona.readAll();
		for (PersonaDTO persona : listadoPersonas)
		{
			if (persona.getTipoContacto().equals(viejoTipoDeContacto))
			{
				persona.setTipoContacto(nuevoTipoDeContacto);
				this.persona.updateTipoDeContacto(persona, nuevoTipoDeContacto);
			}
		}
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	public void borrarLocalidad(LocalidadDTO localidad_a_eliminar) 
	{
		this.localidad.delete(localidad_a_eliminar);
	}
	public void borrarTipoDeContacto(TipoContactoDTO tipo_de_contacto_a_eliminar) 
	{
		this.tipoDeContacto.delete(tipo_de_contacto_a_eliminar);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	public List<PersonaJasperDTO> obtenerPersonasParaReporte()
	{
		// PersonaJasperDTO: mismo que PersonaDTO, pero con año y fecha de nacimiento corregidos.
		// se pasan las personas YA ordenadas segun su tipo.
		List<PersonaDTO> personasOriginales = this.persona.readAll();
		List<PersonaJasperDTO> personasJasper = new ArrayList<PersonaJasperDTO>(personasOriginales.size());
		
		for (PersonaDTO persona : personasOriginales)
		{
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Argentina"));
			cal.setTime(persona.getFechaCumple());
			
			
			String anioCumpleanios = String.valueOf(cal.get(Calendar.YEAR));
			String fechaCumple = new SimpleDateFormat("dd/MM/yyyy").format(persona.getFechaCumple());
			personasJasper.add(new PersonaJasperDTO(persona.getNombre(), persona.getCalle(), persona.getAltura(), persona.getPiso(), persona.getDpto(), persona.getLocalidad(), persona.getEmail(), fechaCumple, anioCumpleanios, persona.getTipoContacto(), persona.getTelefono()));
		}
		// verificamos que hay en la lista Jasper.
		for (PersonaJasperDTO persona : personasJasper)
		{
			System.out.println("nombre: " + persona.getNombre());
			System.out.println("año nacimiento: " + persona.getAnioCumple());
		}
		return personasJasper;
	}
	public List<LocalidadDTO> obtenerLocalidades()
	{
		return this.localidad.readAll();
	}
	public List<TipoContactoDTO> obtenerTiposDeContacto()
	{
		return this.tipoDeContacto.readAll();
	}
	
	public boolean personaExisteEnDB(PersonaDTO persona)
	{
		List<PersonaDTO> personas = this.persona.readAll();
		return personas.contains(persona);
	}
	public boolean localidadExisteEnDB(LocalidadDTO localidad)
	{
		List<LocalidadDTO> localidades = this.localidad.readAll();
		return localidades.contains(localidad);
	}
	public boolean tipoDeContactoExisteEnDB(TipoContactoDTO tipoDeContacto)
	{
		List<TipoContactoDTO> tiposDeContacto = this.tipoDeContacto.readAll();
		return tiposDeContacto.contains(tipoDeContacto);
	}
}